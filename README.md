This addon enables replying to JIRA comments and comment threading.
This works without modifying the comment data storage in JIRA which enables to restore the JIRA functionality by just uninstalling the plugin.
Note the following:
- This does not support previewing wiki markup while comments can include wiki markup

Please be patient for the addon updates for major JIRA releases.
Please avoid writing bad reviews instead of raising a bug report
